//
//  Exercise5App.swift
//  Exercise5
//
//  Created by Danny on 19.07.22.
//

import SwiftUI

@main
struct Exercise5App: App {
    var body: some Scene {
        WindowGroup {
            StartView()
        }
    }
}
