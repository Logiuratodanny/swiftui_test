//
//  ContentView.swift
//  Exercise5
//
//  Created by Danny on 19.07.22.
//

import SwiftUI



struct ContentView: View {
    
    @State var listOfNumbers = NumberList()
    @State var position: Int = 1
    
    var body: some View {
        Text("Full List: " + listOfNumbers.toString())
        Text("Highest Number: " + String(listOfNumbers.max()))
        Text("Lowest Number: " + String(listOfNumbers.min()))
        Text("Mean: " + String(listOfNumbers.getMean()))
        
        Button(action: {
            listOfNumbers.updateList()
        })
        {
            Text("Update Full List")
                .accentColor(Color.black)
                .padding(10)
        }
        .background(Color.red)
        .cornerRadius(10)
        
        TextField("Position", value: $position, formatter: NumberFormatter())
        
        Button(action: {
            listOfNumbers.updateSingleNumber(position: position)
        })
        {
            Text("Update Element of List")
                .accentColor(Color.black)
                .padding(10)
        }
        .background(Color.blue)
        .cornerRadius(10)
    }
}



struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

