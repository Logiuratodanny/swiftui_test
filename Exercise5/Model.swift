//
//  Model.swift
//  Exercise5
//
//  Created by Danny on 19.07.22.
//

import Foundation


struct NumberList{
    private(set) var list: [Int] = []
    private let sizeOfList = 5
    
    private mutating func generateRandomNumber(sizeNumber: Int) -> [Int]{
        var randomlist: [Int] = []
        for _ in 1...sizeNumber {
            let randomInt = Int.random(in: 0..<100)
            randomlist.append(randomInt)
        }
        return randomlist
    }
    
    @discardableResult public mutating func updateList() -> [Int]{
        list = generateRandomNumber(sizeNumber: sizeOfList)
        return list
    }
    
    @discardableResult public mutating func updateSingleNumber(position: Int) -> [Int]{
        if position > 5 || position < 1{
            print("Number not between 1-5")
            return list
        }else{
            let newNumber = Int.random(in: 0..<100)
            self.list[position - 1] = newNumber
            return list
        }
    }
    
    public func max() -> Int{
        guard let max = list.max() else { return 0 }
        return max
    }
    
    public func min() -> Int{
        guard let min = list.min() else { return 0 }
        return min
    }
    
    public func getMean() -> Int{
        let sum = list.reduce(0, +)
        let mean = sum / sizeOfList
        return mean
    }
    
    public func toString() -> String{
        let array = list
        let stringRepresentation = array.description // "1-2-3"
        return stringRepresentation
    }
    
    init(){
        list = generateRandomNumber(sizeNumber: sizeOfList)
    }
}
