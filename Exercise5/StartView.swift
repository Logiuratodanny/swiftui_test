//
//  StartView.swift
//  Exercise5
//
//  Created by Danny on 19.07.22.
//

import SwiftUI

struct StartView: View {
    var body: some View {
        NavigationView{
            NavigationLink(destination: ContentView()){
                ButtonView()
            }
            .navigationTitle("Math for Dummies")
        }
    }
}

struct ButtonView: View {
    var body: some View {
        Text("Start")
            .frame(width: 100, height: 50, alignment: .center)
            .background(Color.red)
            .foregroundColor(Color.black)
            .cornerRadius(20)
    }
}

struct StartView_Previews: PreviewProvider {
    static var previews: some View {
        StartView()
    }
}
